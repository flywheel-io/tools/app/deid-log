# flywheel/deid-log

**The deid-log service is archived as it's no longer used in production.**

De-Identification Logging API enables clients to persist original DICOM tags in
a DB before uploading a de-identified dataset to Flywheel. The deid-log records
can then be queried using SQL based on a set of indexed tags or cross-referenced
with the files uploaded to Flywheel by the deid-log id. Incoming requests are
authenticated with the configured Flywheel Core instance.

```mermaid
sequenceDiagram
    CLIENT  ->>+ DEIDLOG : POST /logs {deid_tags: {...}}
    DEIDLOG ->>+ CORE    : Authorize request
    CORE    ->>- DEIDLOG : Request auth OK
    DEIDLOG ->>- CLIENT  : {id: d34db33f}
    CLIENT  ->>  CORE    : POST /upload {deid_log_id: d34db33f, data: ...}
```

## Usage

Deid-Log supports and is tested with DB backends SQLite, Postgres and MySQL.
Once set up with the desired backend, Flywheel Support will require the host/IP
and the port number of the application as accessible from the local network for
configuring services using it (eg. CLI, reapers and the WEB uploader).

### Running with SQLite

The default DB backend is SQLite and the HTTP API is running on port `8000` - to
expose it on the same port on the host, use:

```bash
docker run --name deid-log -d -p 8000:8000 \
    -e FW_DEID_LOG_CORE_URL=https://local.flywheel.io \
    flywheel/deid-log
```

The default SQLite DB file is stored in a docker volume under `/data` within the
container. It's recommended to let docker manage the storage as in the example
above. To store the sqlite DB in a known host directory instead, bind-mount the
path using `-v /host/data/dir:/data`.

### Running with PostgreSQL

```bash
docker network create deid-net
docker run --network deid-net --name postgres -d \
    -e POSTGRES_DB=db \
    -e POSTGRES_USER=user \
    -e POSTGRES_PASSWORD=pass \
    postgres:12-alpine
docker run --network deid-net --name deid-log -d -p 8000:8000 \
    -e FW_DEID_LOG_DB_URL=postgresql://user:pass@postgres:5432/db \
    -e FW_DEID_LOG_CORE_URL=https://local.flywheel.io \
    flywheel/deid-log
```

### Running with MySQL/MariaDB

```bash
docker network create deid-net
docker run --network deid-net --name mysql -d \
    -e MYSQL_DATABASE=db \
    -e MYSQL_USER=user \
    -e MYSQL_PASSWORD=pass \
    -e MYSQL_ROOT_PASSWORD=root \
    mysql:5  # mariadb:10
docker run --network deid-net --name deid-log -d -p 8000:8000 \
    -e FW_DEID_LOG_DB_URL=mysql://user:pass@mysql:3306/db \
    -e FW_DEID_LOG_CORE_URL=https://local.flywheel.io \
    flywheel/deid-log
```

### Deploying with Helm

Check out the [helm docs](helm/deid-log) for deploying as a chart.

## Querying the deid-log DB

### Schema

Deid-Log auto-creates and uses a single DB table `deid_log`:

```plaintext
sqlite> .schema deid_log
CREATE TABLE deid_log (
    id BINARY(16) NOT NULL,
    created DATETIME,
    timestamp DATETIME,
    "AccessionNumber" VARCHAR(16),
    "PatientBirthDate" VARCHAR(8),
    "PatientID" VARCHAR(64),
    "PatientName" VARCHAR(512),
    "StudyInstanceUID" VARCHAR(64),
    "StudyID" VARCHAR(16),
    "StudyDate" VARCHAR(8),
    "StudyTime" VARCHAR(16),
    "StudyDateTime" VARCHAR(26),
    "StudyDescription" VARCHAR(64),
    "SeriesInstanceUID" VARCHAR(64),
    "SeriesDate" VARCHAR(8),
    "SeriesTime" VARCHAR(16),
    "SeriesDateTime" VARCHAR(26),
    "SeriesDescription" VARCHAR(64),
    "TimezoneOffsetFromUTC" VARCHAR(16),
    "DeidentifiedTags" JSON NOT NULL,
    PRIMARY KEY (id)
);
```

### Find by deid-log ID

```plaintext
sqlite> SELECT PatientName FROM deid_log WHERE id = X'65774D086757457C9DCF96E762803606';
Doe^John
```

### Filter on indexed fields

```plaintext
sqlite> SELECT SeriesInstanceUID FROM deid_log WHERE timestamp < '2006-08-31';
1.2.840.113619.2.62.994044785528.20060823223142485052
```

## Developement

Install the project with dependencies using [poetry](https://python-poetry.org/docs/)
and enable [pre-commit](https://pre-commit.com):

```bash
poetry install
pre-commit install
```

Run locally with debugging and live code reloading enabled:

```bash
FW_DEID_LOG_CORE_URL=https://local.flywheel.io \
FW_DEID_LOG_DB_URL=sqlite:///deid-log.sqlite3 \
gunicorn -c deid_log/config.py -w 1 --reload "deid_log.app:create_app()"
```

To browse the OpenAPI docs, navigate to <http://localhost:8000/docs>.

Integrating upload services need to use
[`POST /logs`](http://localhost:8000/docs#/logs/post_log_logs_post) to send a
payload with both the indexed fields and the deidentified
[DICOM JSON tags](http://dicom.nema.org/medical/dicom/current/output/chtml/part18/sect_F.2.html):

```json
{
  "indexed_fields": {
    "AccessionNumber": "10523475",
    "PatientID": "0000680029",
    "PatientName": "Doe^John"
  },
  "deidentified_tags": {
    "00100010": {"vr": "PN", "Value": [{"Alphabetic": "QA_1TX"}]},
    "00080012": {"vr": "DA", "Value": ["20200505"]},
    "00291020": {"vr": "OB", "InlineBinary": "YQ=="}
  }
}
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
