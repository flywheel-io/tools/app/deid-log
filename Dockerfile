FROM flywheel/python:main.cdd6888e AS deps
# install the prod deps from requirements.txt
EXPOSE 8000
WORKDIR /src
COPY requirements.txt ./
RUN apt-get update -qqy; \
    apt-get install -qqy --no-install-recommends \
        build-essential=12.9 \
        default-libmysqlclient-dev=1.0.7 \
        sqlite3=3.34.1-3 \
    ; \
    pip install --no-cache-dir -r requirements.txt; \
    apt-get purge -qqy --auto-remove \
        build-essential \
    ; \
    rm -rf /var/lib/apt/lists/*; \
    mkdir -p /data; \
    chown -R flywheel:flywheel /data

FROM deps AS dev
# install the dev deps and the package
COPY requirements-dev.txt ./
RUN pip install --no-cache-dir -r requirements-dev.txt
COPY . .
RUN pip install --no-cache-dir --no-deps --editable .

FROM deps as prod
ENV FW_DEID_LOG_DB_URL="sqlite:////data/deid-log.sqlite3"
COPY . .
RUN pip install --no-cache-dir --no-deps --editable .
CMD ["gunicorn", "-c", "deid_log/config.py", "deid_log.app:create_app()"]
VOLUME /data
