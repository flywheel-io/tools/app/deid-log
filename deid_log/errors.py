"""Deid-Log API errors and error handlers."""
import typing as t

from fw_client import errors
from starlette.requests import Request
from starlette.responses import JSONResponse


def http_error_handler(_: Request, exc: errors.RequestException) -> JSONResponse:
    """General requests HTTPError handler."""
    status_code = exc.response.status_code if exc.response is not None else 500
    return JSONResponse({"detail": str(exc)}, status_code=status_code)


exception_handlers: t.Dict[t.Type[Exception], t.Callable] = {  # type: ignore
    errors.RequestException: http_error_handler
}
