"""Deid-Log API endpoint handlers."""
import datetime
import typing as t
import uuid

import sqlalchemy as sqla
from fastapi import APIRouter, Depends, HTTPException, Response
from fw_utils import get_datetime

from .db import deid_log, get_db
from .models import Log, PostLogPayload, PostLogResponse

logs = APIRouter()


@logs.post("", response_model=PostLogResponse)
def create_deid_log(
    payload: PostLogPayload, engine: sqla.engine.Engine = Depends(get_db)
) -> PostLogResponse:
    """Create a new Deid-Log record and return it's ID."""
    log_entry = Log(
        DeidentifiedTags=payload.deidentified_tags,
        **payload.indexed_fields.dict(),
    )
    series_timestamp = get_timestamp(log_entry, "Series")
    study_timestamp = get_timestamp(log_entry, "Study")
    log_entry.timestamp = series_timestamp or study_timestamp
    # pylint: disable=no-value-for-parameter
    insert_op = deid_log.insert().values(**log_entry.dict(exclude_none=True))
    with engine.connect() as conn:
        conn.execute(insert_op)
    return PostLogResponse(id=log_entry.id)


@logs.get(
    "/{log_id}",
    status_code=204,
    responses={404: {"description": "Deid-Log entry does not exist"}},
)
def check_deid_log(
    log_id: uuid.UUID, engine: sqla.engine.Engine = Depends(get_db)
) -> None:
    """Return empty 204 if the Deid-Log ID exists, otherwise 404."""
    with engine.connect() as conn:
        query = sqla.sql.select([deid_log.c.id]).where(deid_log.c.id == log_id)
        result = conn.execute(query).first()
    if result:
        Response(status_code=204)
    else:
        raise HTTPException(404, f"No deid-log record found with id {log_id}")


def get_timestamp(log_entry: Log, tag_prefix: str) -> t.Optional[datetime.datetime]:
    """Get UTC timestamp for the given log entry.

    Try *DateTime field if set otherwise fallback to *Date, *Time, TimezoneOffsetFromUTC
    fields. Omit microseconds to be compatible with all supported database backends.

    References:
    - DA/TM/DT VR types:
      http://dicom.nema.org/dicom/2013/output/chtml/part05/sect_6.2.html
    """
    parsed_dt = None

    if getattr(log_entry, f"{tag_prefix}DateTime"):
        # use *DateTime tag if set
        dt_val = getattr(log_entry, f"{tag_prefix}DateTime")
        parsed_dt = parse_datetime_str(dt_val)

    if not parsed_dt:
        # use *Date and *Time fields otherwise if set
        date = getattr(log_entry, f"{tag_prefix}Date")
        if not date:
            # if no date then we can't do to much so return
            return None
        time = getattr(log_entry, f"{tag_prefix}Time", None) or ""
        offset = log_entry.TimezoneOffsetFromUTC or ""
        parsed_dt = parse_datetime_str(f"{date}{time}{offset}")

    if parsed_dt:
        # omit microseconds
        parsed_dt = parsed_dt.replace(microsecond=0)

    return parsed_dt


def parse_datetime_str(value: str) -> t.Optional[datetime.datetime]:
    """Parse datetime string.

    Args:
        value (str): DICOM DT formatted timestamp: YYYYMMDDHHMMSS.FFFFFF&ZZXX
            The year is required, but everything else has defaults:
            month=day=1, hour=12, minute=second=microsecond=0

    Reference:
        http://dicom.nema.org/dicom/2013/output/chtml/part05/sect_6.2.html
    """
    try:
        dt_obj = get_datetime(f"{value[:8]} {value[8:]}")
    except ValueError:
        return None
    if len(value) < 10:
        dt_obj = dt_obj.replace(hour=12)
    return dt_obj
