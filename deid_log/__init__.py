"""Deid-Log API metadata."""
from importlib import metadata

__version__ = metadata.version(__name__)
