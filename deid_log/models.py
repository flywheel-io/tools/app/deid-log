"""Deid-Log API and domain models: API <=> DOMAIN <=> DB."""
# pylint: disable=no-name-in-module,too-few-public-methods
import datetime
import typing as t
import uuid

from pydantic import BaseModel, Field

from . import const


# Submodels defined first to avoid typing forward references
class IndexedFields(BaseModel):
    """Submodel for DICOM fields that are searchable/indexed."""

    AccessionNumber: t.Optional[str] = Field(
        max_length=const.ACCESSIONNUMBER_LENGTH, example="10523475"
    )
    PatientBirthDate: t.Optional[str] = Field(
        max_length=const.PATIENTBIRTHDATE_LENGTH, example="19641128"
    )
    PatientID: t.Optional[str] = Field(
        max_length=const.PATIENTID_LENGTH, example="0000680029"
    )
    PatientName: t.Optional[str] = Field(
        max_length=const.PATIENTNAME_LENGTH, example="Doe^John"
    )
    StudyInstanceUID: t.Optional[str] = Field(
        max_length=const.STUDYINSTANCEUID_LENGTH,
        example="1.2.840.113619.2.62.994044785528.20060823.200608232232322.9",
    )
    StudyID: t.Optional[str] = Field(
        max_length=const.STUDYID_LENGTH, example="10523475"
    )
    StudyDate: t.Optional[str] = Field(
        max_length=const.STUDYDATE_LENGTH, example="20060823"
    )
    StudyTime: t.Optional[str] = Field(
        max_length=const.STUDYTIME_LENGTH, example="222400"
    )
    StudyDateTime: t.Optional[str] = Field(
        max_length=const.STUDYDATETIME_LENGTH, example="20060823222400"
    )
    StudyDescription: t.Optional[str] = Field(
        max_length=const.STUDYDESCRIPTION_LENGTH, example="Study desc"
    )
    SeriesInstanceUID: t.Optional[str] = Field(
        max_length=const.SERIESINSTANCEUID_LENGTH,
        example="1.2.840.113619.2.62.994044785528.20060823223142485052",
    )
    SeriesDate: t.Optional[str] = Field(
        max_length=const.SERIESDATE_LENGTH, example="20060823"
    )
    SeriesTime: t.Optional[str] = Field(
        max_length=const.SERIESTIME_LENGTH, example="222400"
    )
    SeriesDateTime: t.Optional[str] = Field(
        max_length=const.SERIESDATETIME_LENGTH, example="20060823222400"
    )
    SeriesDescription: t.Optional[str] = Field(
        max_length=const.SERIESDESCRIPTION_LENGTH, example="Series desc"
    )
    TimezoneOffsetFromUTC: t.Optional[str] = Field(
        max_length=const.TIMEZONEOFFSETFROMUTC_LENGTH, example="+0100"
    )


class DcmField(BaseModel):
    """Submodel for any non-indexed DICOM JSON field.

    References:
      - http://dicom.nema.org/medical/dicom/current/output/chtml/part18/sect_F.2.html
      - https://github.com/pydicom/pydicom/blob/v2.0.0/pydicom/dataelem.py#L258
    """

    vr: str
    Value: t.Optional[t.List[t.Any]]
    InlineBinary: t.Optional[str]


# API models
class PostLogPayload(BaseModel):
    """POST /logs payload model (API)."""

    indexed_fields: IndexedFields
    deidentified_tags: t.Dict[str, DcmField] = Field(
        example={
            "00100010": {"vr": "PN", "Value": [{"Alphabetic": "QA_1TX"}]},
            "00080005": {"vr": "CS", "Value": ["ISO_IR 100"]},
            "00080012": {"vr": "DA", "Value": ["20200505"]},
            "00291020": {"vr": "OB", "InlineBinary": "YQ=="},
        }
    )


class PostLogResponse(BaseModel):
    """POST /logs response model (API)."""

    id: uuid.UUID


# Domain model
class Log(BaseModel):
    """Deid-Log model (domain)."""

    id: uuid.UUID = Field(default_factory=uuid.uuid4)
    created: datetime.datetime = Field(default_factory=datetime.datetime.utcnow)
    timestamp: t.Optional[datetime.datetime]
    AccessionNumber: t.Optional[str] = Field(max_length=const.ACCESSIONNUMBER_LENGTH)
    PatientBirthDate: t.Optional[str] = Field(max_length=const.PATIENTBIRTHDATE_LENGTH)
    PatientID: t.Optional[str] = Field(max_length=const.PATIENTID_LENGTH)
    PatientName: t.Optional[str] = Field(max_length=const.PATIENTNAME_LENGTH)
    StudyInstanceUID: t.Optional[str] = Field(max_length=const.STUDYINSTANCEUID_LENGTH)
    StudyID: t.Optional[str] = Field(max_length=const.STUDYID_LENGTH)
    StudyDate: t.Optional[str] = Field(max_length=const.STUDYDATE_LENGTH)
    StudyTime: t.Optional[str] = Field(max_length=const.STUDYTIME_LENGTH)
    StudyDateTime: t.Optional[str] = Field(max_length=const.STUDYDATETIME_LENGTH)
    StudyDescription: t.Optional[str] = Field(max_length=const.STUDYDESCRIPTION_LENGTH)
    SeriesInstanceUID: t.Optional[str] = Field(
        max_length=const.SERIESINSTANCEUID_LENGTH
    )
    SeriesDate: t.Optional[str] = Field(max_length=const.SERIESDATE_LENGTH)
    SeriesTime: t.Optional[str] = Field(max_length=const.SERIESTIME_LENGTH)
    SeriesDateTime: t.Optional[str] = Field(max_length=const.SERIESDATETIME_LENGTH)
    SeriesDescription: t.Optional[str] = Field(
        max_length=const.SERIESDESCRIPTION_LENGTH
    )
    TimezoneOffsetFromUTC: t.Optional[str] = Field(
        max_length=const.TIMEZONEOFFSETFROMUTC_LENGTH
    )
    DeidentifiedTags: t.Dict[str, DcmField]
