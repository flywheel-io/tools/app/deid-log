"""Deid-Log application and gunicorn configuration."""
# pylint: disable=no-member,no-name-in-module,too-few-public-methods
import functools
import logging
import logging.config
import os
import sys

from fw_logging import get_log_config
from pydantic import AnyHttpUrl, BaseSettings, Field, ValidationError

log = logging.getLogger(__name__)


# application config
@functools.lru_cache()
def get_config() -> "Config":
    """Get the app config from envvars (cached)."""
    log.info("Loading configuration")
    try:
        return Config()
    except ValidationError as exc:  # pragma: no cover
        log.critical(exc)
        sys.exit(1)


class Config(BaseSettings):
    """Deid-Log config model."""

    class Config:
        """Enable envvars with prefix 'FW_DEID_LOG_'."""

        env_prefix = "fw_deid_log_"

    api_prefix: str = Field("", regex=r"|/.+[^/]")
    core_url: AnyHttpUrl
    db_url: str
    db_echo: bool = False


# gunicorn config
# see https://docs.gunicorn.org/en/stable/settings.html#settings
# pylint: disable=invalid-name
proc_name = "gunicorn:fw_deid_log"
bind = ["[::]:8000"]
worker_class = "uvicorn.workers.UvicornWorker"
workers = (os.cpu_count() or 1) + 1
accesslog = "-"
errorlog = "-"
loggers = {"gunicorn.access": None, "gunicorn.error": None}
logconfig_dict = get_log_config(loggers=loggers)
logging.config.dictConfig(logconfig_dict)


def on_starting(_):
    """Init the app within gunicorn's on_starting event."""
    # pylint: disable=cyclic-import,import-outside-toplevel
    from deid_log import app

    app.init()
