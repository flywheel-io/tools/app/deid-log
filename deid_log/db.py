"""Database table definitions and connection/schema utils."""
import functools
import logging
import time
import typing as t

import alembic.command
import alembic.config
import sqlalchemy as sqla
from sqlalchemy_utils import UUIDType

from . import const
from .config import get_config

log = logging.getLogger(__name__)


def init() -> None:
    """Wait for DB connection and run schema upgrade if needed."""
    wait_for_connection()
    upgrade_schema()


def wait_for_connection() -> None:
    """Wait for DB connection with an exponential backoff retry strategy."""
    engine = get_db()
    log.info(f"Connecting to {engine.name} DB")
    max_attempts = 7
    # max 7 total attempts with 6 retries, using sleep(2^N) between attempts:
    # try0, sleep(1), try1, sleep(2), ..., try5, sleep(32), try6
    for attempt in range(max_attempts):
        try:
            with engine.connect() as conn:  # pylint: disable=not-context-manager
                conn.execute("SELECT 1")
                return
        except sqla.exc.OperationalError as exc:
            if attempt < max_attempts - 1:
                delay = 2**attempt
                # pylint: disable=protected-access
                log.warning(f"Retrying ({exc._message().strip()})")
                time.sleep(delay)
            else:
                log.error(f"Could not connect to DB in {max_attempts} attempts")
                raise


def upgrade_schema() -> None:
    """Check DB schema with alembic and upgrade if needed."""
    log.info("Checking DB schema version")
    config = alembic.config.Config("alembic.ini")
    alembic.command.upgrade(config, "head")


def get_db() -> sqla.engine.Engine:
    """Get sqla engine for the config DB URL."""
    return _get_db()


@functools.lru_cache()
def _get_db() -> sqla.engine.Engine:
    """Get sqla engine for the config DB URL and cache it."""
    config = get_config()
    log.info(f"Creating DB engine for {config.db_url}")
    kwargs: t.Dict[str, t.Any] = {"echo": config.db_echo}
    # customize engine when using in-memory sqlite to accommodate tests
    if config.db_url == "sqlite:///:memory:":
        kwargs["poolclass"] = sqla.pool.StaticPool
        kwargs["connect_args"] = {"check_same_thread": False}
    return sqla.create_engine(config.db_url, **kwargs)


# SqlAlchemy metadata to register all table definitions with
metadata = sqla.MetaData()

# deid_log table
deid_log = sqla.Table(
    "deid_log",
    metadata,
    sqla.Column("id", UUIDType, primary_key=True),
    sqla.Column("created", sqla.DateTime, index=True),
    sqla.Column("timestamp", sqla.DateTime, index=True),
    sqla.Column(
        "AccessionNumber", sqla.String(const.ACCESSIONNUMBER_LENGTH), index=True
    ),
    sqla.Column(
        "PatientBirthDate", sqla.String(const.PATIENTBIRTHDATE_LENGTH), index=True
    ),
    sqla.Column("PatientID", sqla.String(const.PATIENTID_LENGTH), index=True),
    sqla.Column("PatientName", sqla.String(const.PATIENTNAME_LENGTH), index=True),
    sqla.Column(
        "StudyInstanceUID", sqla.String(const.STUDYINSTANCEUID_LENGTH), index=True
    ),
    sqla.Column("StudyID", sqla.String(const.STUDYID_LENGTH), index=True),
    sqla.Column("StudyDate", sqla.String(const.STUDYDATE_LENGTH), index=True),
    sqla.Column("StudyTime", sqla.String(const.STUDYTIME_LENGTH), index=True),
    sqla.Column("StudyDateTime", sqla.String(const.STUDYDATETIME_LENGTH), index=True),
    sqla.Column(
        "StudyDescription", sqla.String(const.STUDYDESCRIPTION_LENGTH), index=True
    ),
    sqla.Column(
        "SeriesInstanceUID", sqla.String(const.SERIESINSTANCEUID_LENGTH), index=True
    ),
    sqla.Column("SeriesDate", sqla.String(const.SERIESDATE_LENGTH), index=True),
    sqla.Column("SeriesTime", sqla.String(const.SERIESTIME_LENGTH), index=True),
    sqla.Column("SeriesDateTime", sqla.String(const.SERIESDATETIME_LENGTH), index=True),
    sqla.Column(
        "SeriesDescription", sqla.String(const.SERIESDESCRIPTION_LENGTH), index=True
    ),
    sqla.Column(
        "TimezoneOffsetFromUTC", sqla.String(const.SERIESDESCRIPTION_LENGTH), index=True
    ),
    sqla.Column("DeidentifiedTags", sqla.JSON, nullable=False),
)
