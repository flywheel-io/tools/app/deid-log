"""Flywheel Core HTTP client initialization and auth header verification."""
import functools
import logging

from fastapi import Security
from fastapi.security import APIKeyHeader
from fw_client import FWClient

from . import __version__
from .config import get_config

log = logging.getLogger(__name__)


def init() -> None:
    """Check core connection and that the deid_log feature is enabled."""
    client = _get_client()
    log.info("Checking 'deid_log' feature flag in /api/config")
    assert client.check_feature("deid_log"), "Feature flag 'deid_log' is disabled"


def auth(token: str = Security(APIKeyHeader(name="authorization"))) -> dict:
    """Get core's /auth/status response for the authorization header."""
    client = _get_client()
    return client.get("/api/auth/status", auth=token)  # type: ignore


@functools.lru_cache()
def _get_client() -> FWClient:
    """Get core client and cache it."""
    log.info("Creating core client")
    return FWClient(
        url=get_config().core_url,
        client_name="deid-log",
        client_version=__version__,
        defer_auth=True,
    )
