"""Deid-Log constants."""


ACCESSIONNUMBER_LENGTH = 128
PATIENTBIRTHDATE_LENGTH = 64
PATIENTID_LENGTH = 128
PATIENTNAME_LENGTH = 1024
STUDYINSTANCEUID_LENGTH = 128
STUDYID_LENGTH = 128
STUDYDATE_LENGTH = 64
STUDYTIME_LENGTH = 64
STUDYDATETIME_LENGTH = 128
STUDYDESCRIPTION_LENGTH = 1024
SERIESINSTANCEUID_LENGTH = 128
SERIESDATE_LENGTH = 64
SERIESTIME_LENGTH = 64
SERIESDATETIME_LENGTH = 128
SERIESDESCRIPTION_LENGTH = 1024
TIMEZONEOFFSETFROMUTC_LENGTH = 64
