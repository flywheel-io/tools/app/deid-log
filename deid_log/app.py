"""Deid-Log application factory."""
import logging
import sys

from fastapi import APIRouter, Depends, FastAPI, Response
from fw_http_metrics import MetricsMiddleware, get_metrics
from starlette.middleware.cors import CORSMiddleware

from . import __version__, core, db
from .config import get_config
from .errors import exception_handlers
from .handlers import logs

log = logging.getLogger(__name__)


def init() -> None:
    """Init all app dependencies (core and db)."""
    log.info("Initializing deid-log dependencies")
    try:
        core.init()
        db.init()
    except Exception as exc:  # pylint: disable=broad-except
        log.error(exc)
        sys.exit(1)


def create_app() -> FastAPI:
    """Deid-Log application factory."""
    config = get_config()
    log.info("Creating app")
    app = FastAPI(
        title="Flywheel Deid-Log API",
        description="Persist original DICOM tags before de-identification",
        version=__version__,
        docs_url=f"{config.api_prefix}/docs",
        redoc_url=f"{config.api_prefix}/redoc",
        openapi_url=f"{config.api_prefix}/openapi.json",
    )

    # add prometheus middleware to track endpoint usage and performance
    app.add_middleware(MetricsMiddleware, prefix="fw_deid_log_")
    # allow CORS via middleware
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_methods=["*"],
        allow_headers=["*"],
    )

    # add router with config.api_prefix for easy ingress proxying
    router = APIRouter()
    router.include_router(logs, prefix="/logs", dependencies=[Depends(core.auth)])
    router.add_route("/health", lambda _: Response(status_code=204))
    router.add_route("/metrics", get_metrics)
    app.include_router(router, prefix=config.api_prefix)

    # add exception handlers
    for exception, handler in exception_handlers.items():
        app.add_exception_handler(exception, handler)

    return app
