"""Environment module for Alembic."""
# pylint: disable=no-member
import logging.config
import os

from alembic import context

from deid_log.config import get_config
from deid_log.db import get_db, metadata

if os.environ.get("ALEMBIC_CONFIGURE_LOGGING") == "true":
    assert context.config.config_file_name
    logging.config.fileConfig(context.config.config_file_name)


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine. Calls to context.execute()
    here emit the given string to the script output.

    """
    context.configure(
        url=get_config().db_url,
        target_metadata=metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
        compare_type=True,
        render_as_batch=True,
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    engine = get_db()
    with engine.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=metadata,
            compare_type=True,
            render_as_batch=True,
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
