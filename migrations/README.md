# DB schema migrations

[Alembic](https://alembic.sqlalchemy.org/en/latest/tutorial.html) is a
DB migration tool for SQLAlchemy.

## Setup

Setup `FW_DEID_LOG_DB_URL` environment variable.
You can use sqlite or any other supported database.

```sh
export FW_DEID_LOG_DB_URL=sqlite:////tmp/deid-log-migration.sql
export FW_DEID_LOG_CORE_URL=https://local.flywheel.io
```

## Upgrading the DB schema

Upgrade to the latest schema version:

```sh
alembic upgrade head
```

## Creating a new revision

Generate a new DB migration under `migrations/versions`:

```sh
alembic revision --autogenerate --message "schema change description"
```

Edit the auto-generated upgrade if needed:

```sh
alembic edit head
```

Run it by upgrading to the new head:

```sh
alembic upgrade head
```
