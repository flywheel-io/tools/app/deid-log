{{/*
Ingress helper template for supporting multiple K8S versions. Verbatim copy from umbrella:
https://gitlab.com/flywheel-io/infrastructure/umbrella/-/raw/590283b7/helm/flywheel/templates/_ingress.tpl
*/}}

{{/*
Return the appropriate apiVersion for ingress.
*/}}
{{- define "ingress.apiVersion" -}}
{{- if semverCompare "<1.14-0" .Capabilities.KubeVersion.Version -}}
{{- print "extensions/v1beta1" -}}
{{- else if semverCompare "<1.19-0" .Capabilities.KubeVersion.Version -}}
{{- print "networking.k8s.io/v1beta1" -}}
{{- else -}}
{{- print "networking.k8s.io/v1" -}}
{{- end }}
{{- end }}

{{/*
Print "true" if the API pathType field is supported
Usage:
{{ include "ingress.supportsPathType" . }}
*/}}
{{- define "ingress.supportsPathType" -}}
{{- if (semverCompare "<1.18-0" .Capabilities.KubeVersion.Version) -}}
{{- print "false" -}}
{{- else -}}
{{- print "true" -}}
{{- end -}}
{{- end -}}

{{/*
Usage:
{{ include "ingress.pathType" . }}
*/}}
{{- define "ingress.pathType" -}}
{{- if eq "true" (include "ingress.supportsPathType" $) -}}
{{ printf "pathType: %s" (.Values.ingress.pathType | default "ImplementationSpecific") }}
{{- end -}}
{{- end -}}

{{/*
Returns true if the ingressClassname field is supported
Usage:
{{ include "ingress.supportsIngressClassname" . }}
*/}}
{{- define "ingress.supportsIngressClassname" -}}
{{- if semverCompare "<1.18-0" .Capabilities.KubeVersion.Version -}}
{{- print "false" -}}
{{- else -}}
{{- print "true" -}}
{{- end -}}
{{- end -}}

{{/*
Generate backend entry that is compatible with all Kubernetes API versions.
Usage:
{{ include "ingress.backend" (dict "serviceName" "backendName" "servicePort" "backendPort" "context" $) }}

Params:
  - serviceName - String. Name of an existing service backend
  - servicePort - String/Int. Port name (or number) of the service. It will be translated to different yaml depending if it is a string or an integer.
  - context - Dict - Required. The context for the template evaluation.
*/}}
{{- define "ingress.backend" -}}
{{- if ne (include "ingress.apiVersion" .context) "networking.k8s.io/v1" -}}
serviceName: {{ .serviceName }}
servicePort: {{ .servicePort }}
{{- else -}}
service:
  name: {{ .serviceName }}
  port:
    {{- if typeIs "string" .servicePort }}
    name: {{ .servicePort }}
    {{- else if or (typeIs "int" .servicePort) (typeIs "float64" .servicePort) }}
    number: {{ .servicePort | int }}
    {{- end }}
{{- end -}}
{{- end -}}
