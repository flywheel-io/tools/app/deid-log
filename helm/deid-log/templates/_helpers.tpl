{{/* vim: set filetype=mustache: */}}
{{/* Expand the name of the chart. */}}
{{- define "deid-log.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. */}}
{{- define "deid-log.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "deid-log.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Match labels */}}
{{- define "deid-log.matchLabels" -}}
app/name: {{ include "deid-log.name" . }}
app/instance: {{ .Release.Name }}
app/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Common labels */}}
{{- define "deid-log.labels" -}}
helm.sh/chart: {{ include "deid-log.chart" . }}
{{ include "deid-log.matchLabels" . }}
{{- if .Chart.AppVersion }}
app/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end }}
