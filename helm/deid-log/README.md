# deid-log

![Version: 16.14.3](https://img.shields.io/badge/Version-16.14.3-informational?style=flat-square)

Flywheel deid-log service chart for Kubernetes

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| image.repository | string | `"flywheel/deid-log"` | Docker image repository |
| image.tag | string | `"16.14.3"` | Docker image tag |
| image.pullPolicy | string | `"IfNotPresent"` | Docker image pull policy |
| coreUrl | string | `""` | FW Core API URL (required) |
| dbUrl | string | `"sqlite:////data/deid-log.sqlite3"` | Deid-Log database connection URL |
| persistence.size | string | `"100Mi"` | Persistent volume size (for sqlite) |
| persistence.storageClass | string | `nil` | Persistent storage class name |
| servicePort | int | `8000` | Port exposed by the service |
| nodePort | string | `nil` | Static port to expose on the node (if set) |
| ingress.enabled | bool | `false` | Enable an ingress to proxy service requests |
| ingress.domain | string | `nil` | Domain name for the ingress and the certificate |
| ingress.acme | bool | `true` | Use acme automation to get a tls certificate |
| ingress.tlsSecretName | string | `"deid-log-tls"` | The secret name that holds the certificate keys |
| ingress.crt | string | `nil` | The base64 of the certificate to serve |
| ingress.key | string | `nil` | The base64 of the key to serve |
| extraEnv | object | `{}` | Additional environment variables |

## Deployment notes for frontend integration

Deploy to a Kubernetes cluster by defining a `values.yaml` file:

```yaml
image:
  tag: main
  pullPolicy: Always
coreUrl: https://local.flywheel.io  # change to match your core url
nodePort: 30600
ingress:
  enabled: true
  domain: deid-log.dev.flywheel.io
  clusterIssuer: letsencrypt
  tlsSecretName: deid-log
```

A ClusterIssuer must exist on the cluster that can assign certificates using the
ACME protocol. Assuming that the issuer is available run the following to install
`deid-log` with Helm:

```bash
helm upgrade --install --namespace deid-log deid-log ./helm/deid-log -f values.yaml
```

Configure DNS to point to the ingress IP address for the `ingress.domain` value:

```bash
kubectl -n deid-log describe ingress/deid-log | grep Address
```

Your certificate should be issued within a few minutes.
