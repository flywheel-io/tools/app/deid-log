"""Tests for the deid_log.config module."""
from unittest.mock import call

from deid_log.config import on_starting


def test_on_starting_calls_app_init(mocker):
    """App init() should be called on gunicorn startup."""
    app_init = mocker.patch("deid_log.app.init")
    on_starting(None)
    assert app_init.mock_calls == [call()]
