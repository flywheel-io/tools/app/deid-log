#!/usr/bin/env bash
test -z "$TRACE" || set -x
set -eu
USAGE="Usage:
  $0 [IMAGE]...

Run basic smoke tests in a docker container to verify image functionality.
"


main() {
    echo "$*" | grep -Eqvw -- "-h|--help|help" || { echo "$USAGE"; exit; }
    cd tests || die "Cannot cd into tests"
    HOST=$(echo "${DOCKER_HOST:-localhost}" | sed -E "s|tcp://(.*):[0-9]+/?|\1|")
    log "Running compose up"
    trap cleanup INT EXIT
    docker-compose up -d

    log "Waiting for core"
    core() { curl -fLSs -X"$1" "http://$HOST:8080$2" "${@:3}"; }
    for _ in $(seq 5); do  # NOTE curl --retry doesn't cover connreset
        if quiet core GET "/api"; then
            CORE_OK=true && break
        else
            CORE_OK=false && sleep 1
        fi
    done
    test "$CORE_OK" = true || die "core startup failed"
    log "Obtaining core API key"
    AUTH="scitran-user $(core GET /api/devices/self \
        -H"X-SciTran-Auth:secret" \
        -H"X-SciTran-Method:test" \
        -H"X-SciTran-Name:test" \
        | jq -r ".key")"

    log "Waiting for deid-log"
    deid() { curl -X"$1" "http://$HOST:8000$2" "${@:3}"; }
    for _ in $(seq 5); do
        if quiet deid GET "/health"; then
            DEID_OK=true && break
        else
            DEID_OK=false && sleep 1
        fi
    done
    test "$DEID_OK" = true || die "deid-log startup failed"

    log "Running deid-log tests"
    TEMPDIR=$(mktemp -d)
    deid POST "/logs" -H"Authorization:$AUTH" -H"Content-Type:application/json" -d '{
            "indexed_fields": {"PatientID": "test"},
            "deidentified_tags": {"00080012": {"vr": "DA", "Value": ["19991231"]}}
        }' >"$TEMPDIR/post_log.json" \
        || die "Cannot POST /logs"
    LOG_ID=$(jq -r .id "$TEMPDIR/post_log.json")
    deid GET "/logs/$LOG_ID" -H"Authorization:$AUTH" || die "Cannot GET /logs/<id>"
    deid GET "/metrics" >"$TEMPDIR/get_metrics.txt" || die "Cannot GET /metrics"
}


cleanup() {
    EXIT_CODE=$?
    set +e
    if [ "$EXIT_CODE" != 0 ]; then
        err "Tests failed - dumping container logs"
        docker-compose logs core
        docker-compose logs deid-log
    fi
    log "Running compose down"
    quiet docker-compose down -v
    exit $EXIT_CODE
}


# logging and formatting utilities
log() { printf "\e[32mINFO\e[0m %s\n" "$*" >&2; }
err() { printf "\e[31mERRO\e[0m %s\n" "$*" >&2; }
die() { err "$@"; exit 1; }
quiet() { "$@" >/dev/null 2>&1; }


main "$@"
