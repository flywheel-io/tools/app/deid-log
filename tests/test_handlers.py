"""Tests for the deid_log.handlers module."""
import datetime
import uuid

import pytest
import sqlalchemy as sqla

from deid_log import db


def test_logs_post_handler_403(as_public):
    """Test /logs POST handler cannot be accessed without auth."""
    payload = {
        "indexed_fields": {"StudyInstanceUID": "1.2"},
        "deidentified_tags": {
            "00100010": {"Value": [{"Alphabetic": "Patient^Name"}], "vr": "PN"}
        },
    }

    resp = as_public.post("/logs", json=payload)

    assert resp.status_code == 403


@pytest.mark.parametrize(
    "payload",
    [
        {},
        {"indexed_fields": {}},
        {"deidentified_tags": {}},
        {"indexed_fields": {}, "deidentified_tags": {"00000000": {}}},
        {
            "indexed_fields": {},
            "deidentified_tags": {"00000000": {"Value": ["something"]}},
        },
    ],
)
def test_logs_post_handler_422(payload, as_user):
    """Test /logs POST handler with invalid payload."""
    resp = as_user.post("/logs", json=payload)

    assert resp.status_code == 422


def test_logs_post_handler_201(as_user, engine):
    """Test /logs POST handler with valid payload."""
    payload = {
        "indexed_fields": {"StudyInstanceUID": "1.2"},
        "deidentified_tags": {
            "00100010": {"Value": [{"Alphabetic": "Patient^Name"}], "vr": "PN"}
        },
    }
    resp = as_user.post("/logs", json=payload)

    assert resp.status_code == 200
    inserted_id = resp.json()["id"]
    # verfiy that the inserted id is a valid UUID
    assert uuid.UUID(inserted_id)

    # also verify that it is persisted in the database
    with engine.connect() as conn:
        result = conn.execute(
            sqla.sql.select(
                [
                    db.deid_log.c.id,
                    db.deid_log.c.created,
                    db.deid_log.c.StudyInstanceUID,
                    db.deid_log.c.DeidentifiedTags,
                ]
            ).where(db.deid_log.c.id == inserted_id)
        ).first()

    assert result.id == uuid.UUID(inserted_id)
    assert isinstance(result.created, datetime.datetime)
    assert result.StudyInstanceUID == payload["indexed_fields"]["StudyInstanceUID"]
    assert result.DeidentifiedTags == payload["deidentified_tags"]


def test_logs_get_handler_403(as_public):
    """Test /logs/{log_id} endpoint cannot be accessed publicly."""
    resp = as_public.get(f"/logs/{uuid.uuid4()}")

    assert resp.status_code == 403


def test_logs_get_handler_204(as_user):
    """Test /logs/{log_id} endpoint when log exists with the given id."""
    payload = {
        "indexed_fields": {"StudyInstanceUID": "1.2"},
        "deidentified_tags": {
            "00100010": {"Value": [{"Alphabetic": "Patient^Name"}], "vr": "PN"}
        },
    }
    resp = as_user.post("/logs", json=payload)
    assert resp.status_code == 200
    log_id = resp.json()["id"]

    resp = as_user.get(f"/logs/{log_id}")

    assert resp.status_code == 204
    assert not resp.content


def test_logs_get_handler_404(as_user):
    """Test /logs/{log_id} endpoint when log does not exist with the given id."""
    resp = as_user.get(f"/logs/{uuid.uuid4()}")

    assert resp.status_code == 404


def test_logs_get_handler_422(as_user):
    """Test /logs/{log_id} endpoint with invalid UUID."""
    resp = as_user.get("/logs/invalid")

    assert resp.status_code == 422


@pytest.mark.parametrize(
    "payload,expected",
    [
        (
            {"indexed_fields": {"StudyDate": "19900101"}, "deidentified_tags": {}},
            datetime.datetime(1990, 1, 1, 12, 0),
        ),
        (
            {
                "indexed_fields": {"StudyDateTime": "19900101010101"},
                "deidentified_tags": {},
            },
            datetime.datetime(1990, 1, 1, 1, 1, 1),
        ),
        (
            # fallback to *Date if *DateTime value is wrong
            {
                "indexed_fields": {"StudyDateTime": "wrong", "StudyDate": "19900101"},
                "deidentified_tags": {},
            },
            datetime.datetime(1990, 1, 1, 12, 0, 0),
        ),
        (
            # uses *Date and *Time fields if not *DateTime present
            {
                "indexed_fields": {"StudyDate": "19900202", "StudyTime": "020202"},
                "deidentified_tags": {},
            },
            datetime.datetime(1990, 2, 2, 2, 2, 2),
        ),
        (
            # uses *Date, *Time and TimezoneOffsetFromUTC fields
            {
                "indexed_fields": {
                    "StudyDate": "19900101",
                    "StudyTime": "020201",
                    "TimezoneOffsetFromUTC": "+0101",
                },
                "deidentified_tags": {},
            },
            datetime.datetime(1990, 1, 1, 1, 1, 1),
        ),
        (
            # Series* fields have higher precedence
            {
                "indexed_fields": {
                    "StudyDate": "19900101",
                    "StudyTime": "010101",
                    "SeriesDate": "19900202",
                    "SeriesTime": "020202",
                },
                "deidentified_tags": {},
            },
            datetime.datetime(1990, 2, 2, 2, 2, 2),
        ),
        (
            # ignore fractional part and offset
            {
                "indexed_fields": {"StudyDateTime": "19900101010101.000001+0101"},
                "deidentified_tags": {},
            },
            datetime.datetime(1990, 1, 1, 0, 0, 1),
        ),
        (
            # ignore fractional part and negative offset
            {
                "indexed_fields": {"StudyDateTime": "19900101010101.000001-0101"},
                "deidentified_tags": {},
            },
            datetime.datetime(1990, 1, 1, 2, 2, 1),
        ),
        (
            # wrong field values
            {
                "indexed_fields": {
                    "StudyDateTime": "wrong",
                    "StudyDate": "wrong",
                    "StudyTime": "wrong",
                    "SeriesDateTime": "wrong",
                    "SeriesDate": "wrong",
                    "SeriesTime": "wrong",
                },
                "deidentified_tags": {},
            },
            None,
        ),
        (
            # invalid number in datetime
            {"indexed_fields": {"StudyDateTime": "19900000"}, "deidentified_tags": {}},
            None,
        ),
        (
            # *Date not set, but *Time is set
            {
                "indexed_fields": {
                    "StudyTime": "000000",
                },
                "deidentified_tags": {},
            },
            None,
        ),
    ],
)
def test_logs_post_handler_date_parsing(payload, expected, as_user, engine):
    """Test that date parsing works as expected in the expected precedence order."""
    resp = as_user.post("/logs", json=payload)

    assert resp.status_code == 200
    log_id = resp.json()["id"]

    # verify the persisted date value in the database
    with engine.connect() as conn:
        result = conn.execute(
            sqla.sql.select([db.deid_log.c.id, db.deid_log.c.timestamp]).where(
                db.deid_log.c.id == log_id
            )
        ).first()
    assert result.timestamp == expected


def test_logs_invalid_api_key(as_user):
    """Returns 401 in case of wrong api-key."""
    as_user.headers["authorization"] = f"{as_user.headers['authorization']}wrong"

    resp = as_user.post("/logs", json={})

    assert resp.status_code == 401


def test_logs_session_token_w_core_url(as_user, core):
    """Endpoint works fine with session-token and X-FW-CORE-URL header."""
    payload = {
        "indexed_fields": {"StudyInstanceUID": "1.2"},
        "deidentified_tags": {
            "00100010": {"Value": [{"Alphabetic": "Patient^Name"}], "vr": "PN"}
        },
    }
    as_user.headers["authorization"] = "session-token"
    as_user.headers["x-fw-core-url"] = core.url

    resp = as_user.post("/logs", json=payload)

    assert resp.status_code == 200
