"""Common pytest config and shared fixtures."""
# pylint: disable=invalid-name,redefined-outer-name
import os

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import inspect

from deid_log.app import create_app
from deid_log.config import get_config
from deid_log.db import get_db, metadata

pytest_plugins = "fw_http_testserver"
os.environ["TZ"] = "UTC"


def pytest_generate_tests(metafunc):
    """Parametrize tests with db_url parameter from envvar TEST_DB_URLS."""
    if "db_url" in metafunc.fixturenames:
        db_urls = os.getenv("TEST_DB_URLS", "sqlite:///:memory:").split()
        metafunc.parametrize("db_url", db_urls)


@pytest.fixture(autouse=True)
def config(db_url, core, mocker):
    """Fixture that returns the test config instance."""
    env = dict(
        FW_DEID_LOG_DB_URL=db_url,
        FW_DEID_LOG_CORE_URL=core.url,
    )
    mocker.patch.dict(os.environ, env)
    get_config.cache_clear()
    return get_config()


@pytest.fixture
def core(http_testserver):
    return http_testserver


@pytest.fixture
def engine(mocker):
    """Fixture that returns db engine and cleans up automatically."""
    engine_ = get_db()
    mocker.patch("deid_log.db._get_db", return_value=engine_)
    yield engine_
    metadata.drop_all(engine_)
    with engine_.connect() as conn:
        for table_name in inspect(engine_).get_table_names():
            conn.execute(f"DROP TABLE {table_name}")
    engine_.dispose()


@pytest.fixture
def get_table_names(engine):
    """Return lambda for getting the current set of table names in the DB."""
    return lambda: set(inspect(engine).get_table_names())


@pytest.fixture
def app(engine):
    """Fixture that returns an app instance configured for testing."""
    test_app = create_app()
    metadata.create_all(engine)
    yield test_app


@pytest.fixture
def as_public(app) -> TestClient:
    """Fastapi test client without Authorization header."""
    return TestClient(app)


@pytest.fixture
def as_user(app, core) -> TestClient:
    """Fastapi test client with Authorization header set."""
    # register /api/auth/status handler in the test-server
    def auth_status_callback():
        key = core.request.headers.get("Authorization")
        key = key.replace("scitran-user ", "")
        if key in [f"{core.addr}:api-key", "session-token"]:
            resp = {
                "user_id": "user@flywheel.io",
                "origin": {"type": "user", "id": "user@flywheel.io"},
                "user_is_admin": False,
                "is_device": False,
                "roles": ["public", "user"],
            }
            return (resp, 200)
        return ({}, 401)

    core.add_callback("/api/auth/status", auth_status_callback)
    client = TestClient(app)
    client.headers["Authorization"] = f"scitran-user {core.addr}:api-key"
    return client
