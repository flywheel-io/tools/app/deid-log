"""Tests for the deid_log.core module."""
import pytest

import deid_log.core


@pytest.fixture(autouse=True)
def clear_cache(config):  # pylint: disable=unused-argument
    """Ensure tests use a fresh core client with the test config."""
    deid_log.core._get_client.cache_clear()  # pylint: disable=protected-access


def test_init_passes_when_deid_feature_flag_is_set(core):
    """Init should check core's enabled feature for GET /api/config."""
    core.add_response("/api/config", {"features": {"deid_log": True}})
    assert deid_log.core.init() is None


def test_init_raises_when_deid_feature_flag_is_not_set(core):
    """Init should raise if the deid_log feature is not enabled."""
    core.add_response("/api/config", {"features": {"deid_log": False}})
    with pytest.raises(AssertionError):
        deid_log.core.init()
