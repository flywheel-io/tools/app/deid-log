"""Tests for the deid_log.app module."""
from unittest.mock import call

import pytest
from fastapi.testclient import TestClient

from deid_log import app


def test_init_calls_core_and_db(mocker):
    """App init should call core.init() and db.init()."""
    core_init = mocker.patch("deid_log.core.init")
    db_init = mocker.patch("deid_log.db.init")

    app.init()

    assert core_init.mock_calls == [call()]
    assert db_init.mock_calls == [call()]


def test_init_exits_if_a_dependency_check_fails(mocker):
    """App init should exit(1) if a dependency check fails."""
    mocker.patch("deid_log.core.init", side_effect=Exception)
    with pytest.raises(SystemExit):
        app.init()


def test_api_prefix(config):
    """Test that routes (incl. OpenAPI docs) are prefixed as configured."""
    config.api_prefix = "/prefix"
    client = TestClient(app.create_app())

    assert client.get("/prefix/docs")
    assert client.get("/prefix/openapi.json")
    assert client.get("/prefix/health")


def test_cors_access_control_allow_headers(as_public):
    """Test that access-control-allow-* headers are set via the middleware."""
    as_public.headers["origin"] = "http://host:5000"
    as_public.headers["access-control-request-method"] = "POST"
    as_public.headers["access-control-request-headers"] = "authorization"
    resp = as_public.options("/logs")
    assert resp.status_code == 200
    assert resp.headers["access-control-allow-headers"] == "authorization"
    assert resp.headers["access-control-allow-origin"] == "*"
    assert resp.headers["access-control-allow-methods"] == (
        "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT"
    )


def test_get_health_returns_204(as_public):
    """Test that /health (readiness probe) endpoint returns 204 OK response."""
    resp = as_public.get("/health")
    assert resp.status_code == 204
    assert resp.content == b""
