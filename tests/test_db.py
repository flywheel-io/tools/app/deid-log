"""Tests for the deid_log.db module."""
from unittest.mock import call

import pytest
import sqlalchemy as sqla

from deid_log import db


def test_get_db_caches_result(config):  # pylint: disable=unused-argument
    """The DB engine should be cached (reused) for performance."""
    assert db.get_db() is db.get_db()


def test_init_calls_wait_and_upgrade(mocker):
    """Init should call wait_for_connection() and upgrade_schema()."""
    wait_for_connection = mocker.patch("deid_log.db.wait_for_connection")
    upgrade_schema = mocker.patch("deid_log.db.upgrade_schema")

    db.init()

    assert wait_for_connection.mock_calls == [call()]
    assert upgrade_schema.mock_calls == [call()]


def test_wait_for_connection_retries(mocker):
    """Test wait_for_connection() retries to connect after a sleep."""
    # mock sleep to not have to actually wait
    sleep_mock = mocker.patch("time.sleep")
    # mock engine raising on the 1st attempt but passing on the 2nd
    engine_mock = mocker.patch("deid_log.db._get_db")
    connect_mock = engine_mock().connect.return_value.__enter__.return_value
    connect_mock.execute.side_effect = [
        sqla.exc.OperationalError(None, None, None),
        None,
    ]

    db.wait_for_connection()

    assert len(connect_mock.mock_calls) == 2
    assert sleep_mock.mock_calls == [call(1)]


def test_wait_for_connection_raises_after_max_attemps(mocker):
    """Test wait_for_connection() bails after 7 tries (w/ exponential backoff)."""
    sleep_mock = mocker.patch("time.sleep")
    # mock engine that that always raises on connection attempts
    engine_mock = mocker.patch("deid_log.db._get_db")
    connect_mock = engine_mock().connect.return_value.__enter__.return_value
    connect_mock.execute.side_effect = sqla.exc.OperationalError(None, None, None)

    with pytest.raises(sqla.exc.OperationalError):
        db.wait_for_connection()

    assert len(connect_mock.mock_calls) == 7
    assert sleep_mock.mock_calls == [
        call(1),
        call(2),
        call(4),
        call(8),
        call(16),
        call(32),
    ]


def test_upgrade_schema_creates_tables_on_empty_db(get_table_names):
    """Test upgrade_schema creates tables successfully on an empty DB."""
    assert get_table_names() == set()
    db.upgrade_schema()
    assert get_table_names() == {"alembic_version", "deid_log"}


def test_upgrade_schema_passes_on_up_to_date_db(get_table_names):
    """Test upgrade_schema can be called on ."""
    db.upgrade_schema()
    db.upgrade_schema()
    assert get_table_names() == {"alembic_version", "deid_log"}
